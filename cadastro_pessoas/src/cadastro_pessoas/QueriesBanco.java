/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastro_pessoas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Douglas
 */
public class QueriesBanco {

    public void conectarAoBanco(String senha, String usuario) {

        try {

            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", usuario, senha);
            System.out.println("Conectou!");
            conn.close();

        } catch (SQLException e) {

            System.out.println("Não conectou!");
            e.printStackTrace();
        }

    }

    /*
    public void ssalvarDadosPessoaFisica(String cpf, String nome, String endereco, String cep, String uf, String municipio, String telefone_residencial, String telefone_celular, String email) {

        try {

            Connection conn = conectar();

            Statement stm = conn.createStatement();

            String sql = "INSERT INTO pessoa_fisica (id_pessoa, cpf, nome, endereco, cep, uf, municipio, telefone_residencial, telefone_celular, email) VALUES (pessoa_seq.nextval," + cpf + ", '" + nome + "', '" + endereco + "', " + cep + ", '" + uf + "', '" + municipio + "', " + telefone_residencial + "," + telefone_celular + ", '" + email + "')";

            System.out.println(sql);

            stm.executeQuery(sql);

            conn.commit();

            conn.close();

        } catch (SQLException e) {

            System.out.println("Não conectou!");
            e.printStackTrace();
        }

    }
     */
    public boolean salvarDadosPessoaFisica(String cpf, String nome, String endereco, String cep, String uf, String municipio, String telefone_residencial, String telefone_celular, String email) {

        try {

            Connection conn = conectar();

            Statement stm = conn.createStatement();

            String sql = "INSERT INTO pessoa_fisica "
                    + "(id_pessoa, cpf, nome, endereco, cep, uf, municipio, telefone_residencial, telefone_celular, email) "
                    + "VALUES (pessoa_seq.nextval," + cpf + ", '" + nome + "', '" + endereco + "', " + cep + ", '" + uf + "', '"
                    + municipio + "', " + telefone_residencial + "," + telefone_celular + ", '" + email + "')";

            System.out.println(sql);

            stm.executeQuery(sql);

            conn.commit();

            conn.close();

            return true;

        } catch (SQLException e) {

            System.out.println("Não salvou!");

            e.printStackTrace();

            return false;

        }

    }
    
    public void pesquisaCadastroPessoaFisica(String nome){
        
        try{
            
            Connection conn = conectar();

            Statement stm = conn.createStatement();
            
            String sql = "select * from pessoa_fisica where nome = '" + nome + "'";
            
            ResultSet rs = stm.executeQuery(sql);
            
            while(rs.next()){
            String[] linha = {rs.getString("ID_PESSOA"), rs.getString("CPF"),
                    rs.getString("NOME"),rs.getString("ENDERECO"),rs.getString("CEP"),
                    rs.getString("UF"),rs.getString("MUNICIPIO"), rs.getString("TELEFONE_RESIDENCIAL"), rs.getString("TELEFONE_CELULAR"), rs.getString("EMAIL") };
            }
          
            
        }catch (SQLException e){
            
           
            
        }
        
        
        
    }
    
    public boolean salvarNovoUsuario(String usuario, String senha) {

        try {

            Connection conn = conectar();

            Statement stm = conn.createStatement();

            String sql = "INSERT INTO usuario "
                    + "(id_usuario, usuario, senha, num_acessos, ativo) "
                    + "VALUES (usuario_seq.nextval, '" + usuario + "', '" + senha + "', " + 0 + ", " + 1 + ")";

            System.out.println(sql);

            stm.executeQuery(sql);

            conn.commit();

            conn.close();

            return true;

        } catch (SQLException e) {

            System.out.println("Não salvou!");

            e.printStackTrace();

            return false;

        }

    }
    
    public boolean atualizaSenha(String usuario, String novaSenha) {

        try {

            Connection conn = conectar();

            Statement stm = conn.createStatement();

            String sql = "update usuario set senha = '" + novaSenha + "' where usuario = '" + usuario + "'";
         
            stm.executeQuery(sql);

            conn.commit();

            conn.close();
            
            return true;
                       
        } catch (SQLException e) {

            System.out.println("Não salvou!");

            e.printStackTrace();

            return false;
        }

       
        
    }
    
    public boolean editaRegistro(String id_pessoa, String cpf, String nome, String endereco, String cep, String uf, String municipio, String telefone_residencial, String telefone_celular, String email){
        
        try {

            Connection conn = conectar();

            Statement stm = conn.createStatement();

            String sql = "update pessoa_fisica set cpf = " + cpf + ", nome = '" + nome + "', " + "endereco = '" + endereco + "', " + "cep = " + cep + ", " + "uf = '" + uf + "', " + "municipio = '" + municipio + "', " + "telefone_residencial = " + telefone_residencial 
                    + ", " + "telefone_celular = " + telefone_celular + ", " + "email = '" + email + "' where id_pessoa = " + id_pessoa;

            System.out.println(sql);
            
            stm.executeQuery(sql);

            conn.commit();

            conn.close();

            return true;

        } catch (SQLException e) {

            System.out.println("Não salvou!");

            e.printStackTrace();

            return false;

        }
        
    }

    public boolean incrementaNumeroAcessos(String usuario) {

        try {

            Connection conn = conectar();

            Statement stm = conn.createStatement();

            String sql = "update usuario set num_acessos = num_acessos+1 where usuario = '" + usuario + "'";

            stm.executeQuery(sql);

            conn.commit();

            conn.close();

            return true;

        } catch (SQLException e) {

            System.out.println("Não salvou!");

            e.printStackTrace();

            return false;

        }

    }

    public boolean verificaNumeroAcessos(String usuario) {

        try {

            Connection conn = conectar();

            Statement stm = conn.createStatement();

            String sql = "select num_acessos from usuario where usuario = '" + usuario + "'";

            ResultSet rs = stm.executeQuery(sql);

           
            if (rs.next()) {

                int numeroAcessos = rs.getInt("num_acessos");

                if (numeroAcessos <= 0) {

                    return true;

                } else {

                    return false;
                }

            }

        } catch (SQLException e) {

            e.printStackTrace();

            return false;

        }

        return false;
    }

    public static Connection conectar() throws SQLException {
        return DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "desenvolvimento", "desenvolvimento");
    }

    public void desconectaBanco() {

    }

    public boolean validaUsuarioEsenha(String usuarioInserido, String senhaInserida) throws SQLException {

        try {

            Connection conn = conectar();

            Statement stm = conn.createStatement();

            String sql = "select * from usuario where usuario = '" + usuarioInserido + "' and senha = '" + senhaInserida + "'";

            ResultSet rs = stm.executeQuery(sql);

            if (rs.next()) {

                return true;

            } else {

                return false;
            }

        } catch (SQLException e) {

            System.out.println("caiu no catch");
            e.printStackTrace();

        }

        return false;

    }

    public boolean excluirRegistro(String id) {
       
        try {

            Connection conn = conectar();

            Statement stm = conn.createStatement();

            String sql = "delete from pessoa_fisica where id_pessoa = " + id;

            ResultSet rs = stm.executeQuery(sql);

          
            return true;
            

        } catch (SQLException e) {

            System.out.println("caiu no catch");
            e.printStackTrace();

        }

        return false;
        
    }
}
